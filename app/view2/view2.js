'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2/:id', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl',
    controllerAs : 'vm'
  });
}]).controller('View2Ctrl', Ctrl);

function Ctrl($http, $routeParams) {
    var vm = this;
    this.item = getItems($routeParams.id);
    this.add = add;
    this.removeItem = removeItem;

    function init() {
        $http.get('https://localhost:44394/api/Vehicles/GetByPersonId?id=' + $routeParams.id).then(function (result) {
            vm.items = result.data;
        });
    }

    function add() {
        var item = this.item;
        this.item.personId = $routeParams.id;
        $http.post('https://localhost:44394/api/Vehicles/AddVehicle', item).then(init);
        this.item = null;
    }

    function removeItem(id) {
            $http.delete('https://localhost:44394/api/Vehicles/' + id)
            .then(init);
    }

    function getItems(id) {
        if (id === undefined) {
            return null;
        }
        $http.get('https://localhost:44394/api/Vehicles/GetByPersonId?id=' + id).then(function (result) {
            vm.items =  result.data;
        });
    }
}