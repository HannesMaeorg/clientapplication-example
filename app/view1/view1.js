'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl',
    controllerAs : 'vm'
  });
}]).controller('View1Ctrl', Ctrl);

function Ctrl($http) {
    var vm = this;
    this.add = add;
    this.removeItem = removeItem;
    this.markHidden = markHidden;
    this.searchByName = searchByName;
    this.searchByCode = searchByCode;
    init();

    function init() {
        $http.get('https://localhost:44394/api/People').then(function (result) {
            vm.items = result.data;
        });
    }

    function add() {
        var item = this.item;
        $http.post('https://localhost:44394/api/People/AddPerson', item).then(init);
        this.item = null;
        $location.path("/view1");
    }

    function removeItem(id) {
        $http.delete('https://localhost:44394/api/People/' + id)
        .then(init);
    }

    function markHidden(id) {
        $http.put('https://localhost:44394/api/People/MarkHidden?id=' + id)
        .then(init);
    }

    function searchByName(){
        var name = document.getElementById("inputName").value;
        if (name === ''){
            init();
        } else {
            $http.get('https://localhost:44394/api/People/FindByName?name=' + name).then(function (result) {
                vm.items = result.data;
            })
            document.getElementById("inputName").value = "";
        }
    }

    function searchByCode(){
        var code = document.getElementById("inputCode").value;
        if (code === ''){
            init();
        } else {
            $http.get('https://localhost:44394/api/People/GetByPersonalCode?code=' + code).then(function (result) {
                vm.items = [];
                vm.items.push(result.data);
            })
            document.getElementById("inputCode").value = "";
        }
    }
}
